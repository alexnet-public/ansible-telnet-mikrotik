#  ansible-telnet-mikrotik

```
# MikroTik Device Management Playbook

This playbook automates executing commands on MikroTik devices using SSH or Telnet.

## Prerequisites

- Ansible installed.
- Credentials for MikroTik devices.

## Inventory Example

```ini
[Mikrotik]
router3 ansible_host=192.168.1.231 ansible_user=admin ansible_password=admin ansible_network_os=mikrotik_command ansible_connection=local mikrotik_command_method=ssh mikrotik_command_port=22 ansible_python_interpreter="/usr/bin/python3"
router2 ansible_host=192.168.1.110 ansible_user=admin ansible_password=admin ansible_network_os=mikrotik_command ansible_connection=local mikrotik_command_method=telnet mikrotik_command_port=23 ansible_python_interpreter="/usr/bin/python3"


```

## Usage

```bash
pip3 install rancidcmd
ansible-playbook -i hosts mikrotik_test.yaml
```

## Playbook Overview

1. **Execute Commands:** Runs commands (`interface print`, `ip address print`, `ip arp print`) on MikroTik, supporting both SSH and Telnet.
2. **Save Output:** Outputs from commands are saved to `<hostname>.txt`.

## Variables

- `ansible_user`: Username for device access.
- `ansible_password`: Password for device access.
- `enable_password`: Enable password if needed.
- `mikrotik_command_port`: Port for SSH (default 22) or Telnet (default 23).
- `mikrotik_command_method`: `ssh` or `telnet` for command execution.

Ensure `mikrotik_command_method` matches your device setup. For SSH, ensure SSH access is configured on the MikroTik device.
```

https://pypi.org/project/rancidcmd/


