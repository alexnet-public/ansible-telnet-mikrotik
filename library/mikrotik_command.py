# -*- coding: utf-8 -*-
#!/usr/bin/python3
import importlib.util
import sys
import re
from ansible.module_utils.basic import AnsibleModule
#from mikrotik import MikroTikConnection


def import_module_from_path(name, path):
    spec = importlib.util.spec_from_file_location(name, path)
    module = importlib.util.module_from_spec(spec)
    sys.modules[name] = module
    spec.loader.exec_module(module)
    return module

# Используйте функцию для импорта вашего модуля
mikrotik = import_module_from_path('mikrotik', './library/mikrotik.py')


def run_module():
    module_args = dict(
        user=dict(type='str', required=True),
        password=dict(type='str', required=True, no_log=True),
        enable_password=dict(type='str', required=True, no_log=True),
        addr=dict(type='str', required=True),
        port=dict(type='str', required=False, default='22'),
        method=dict(type='str', required=False, default='ssh'),
        cmd=dict(type='list', required=True, elements='str'),
    )

    result = dict(
        changed=False,
        original_message='',
        message='',
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    if module.check_mode:
        module.exit_json(**result)

    try:
        # Создание экземпляра класса MikroTikConnection
        connector = mikrotik.MikroTikConnection()
        txt = connector.mtlogin(
            module.params['user'],
            module.params['addr'],
            module.params['cmd'],
            module.params['password'],
            module.params['enable_password'],
            module.params['port'],
            module.params['method']
        )
        #save_output_to_file(txt)
        #txt = remove_ansi_escape_sequences(txt)
        #bytes_ansi = txt.encode('cp1252')
        #txt = bytes_ansi.decode('utf-8')
        #txt = remove_ansi_escape_sequences(txt)
        result['message'] = txt
        
    except Exception as e:
        module.fail_json(msg=str(e), **result)

    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()
