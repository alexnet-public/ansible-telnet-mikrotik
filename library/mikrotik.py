#!/usr/bin/python3
from rancidcmd import RancidCmd
import re
import os
import sys

class MikroTikConnection:


    # Подключиться к консоли микротик
    def mtlogin(self,user,addr,cmd,password,enable_password,port,method):    
        args = dict(user=user,
                    password=password,
                    enable_password=enable_password,
                    login='mtlogin',
                    address=addr,
                    port=port,
                    method=method)
        
        #cmd = ['interface print', 'ip address print', 'ip arp print']
        cmd_string = ''
        i = 0
        while i < len(cmd):
            cmd_string += cmd[i]
            if i < len(cmd) - 1:  # Добавляем экранированный разделитель после всех команд, кроме последней
                cmd_string += '\\r\\n'
            i += 1
        cmd_string += '\\r\\n'  # Добавляем экранированный перенос строки в конец строки cmd_string

        #print(cmd_string)

        rancid = RancidCmd(**args)
        res = rancid.execute(cmd_string)
        #print(res)
 

        if res['rtn_code'] == 0:
            txt = res['std_out']
            return txt
        else:
            #txt = '[error] %s' + res['std_err']
            txt = 'error'
            return txt


    # Исходная функция, с некоторыми корректировками
    def version(self,user, addr, password, enable_password):
        cmd = ['system routerboard print','system resource print','system identity print']
        methods_ports = [('telnet', '23'), ('ssh', '22'), ('ssh', '6522')]
        
        for method, port in methods_ports:
            txt = self.mtlogin(user, addr, cmd, password, enable_password, port, method)
            if txt != 'error':
                #print(txt)
                result = self.parse_mikrotik_output(txt,method,port)
                
                # Добавляем метод и порт к результату перед возвращением
                result.update({'method': method, 'port': port})
                return result
        
        # Если все попытки подключения неудачны
        return {'test': 'error', 'vendor': 'unknown', 'version': '', 'model': '', 'hostname': '', 'method': '', 'port': ''}


    def parse_mikrotik_output(self,text, method, port):
        lines = text.splitlines()  # Разделение текста на строки
        identity, version, model = "Не найдено", "Не найдено", "Не найдено"

        # Флаги для определения, когда начинается вывод каждой команды
        reading_identity, reading_version, reading_model = False, False, False

        for line in lines:
            if 'system routerboard print' in line:
                reading_model = True
            elif 'system resource print' in line:
                reading_version = True
            elif 'system identity print' in line:
                reading_identity = True
            elif reading_identity and line.strip().startswith('name:'):
                identity = line.split(':')[1].strip()
                reading_identity = False  # Прекращаем чтение после нахождения
            elif reading_version and line.strip().startswith('version:'):
                version = line.split(':')[1].strip()
                reading_version = False  # Прекращаем чтение после нахождения
            elif reading_model and line.strip().startswith('board-name:'):
                model = line.split(':')[1].strip()
                reading_model = False  # Прекращаем чтение после нахождения

        return {
            'test': 'ok',
            'vendor': 'Mikrotik',
            'version': version,
            'model': model,
            'hostname': identity,
            'method': method,
            'port': port
        }



# TEST
#################################################
#
#connector = MikroTikConnection()
#
#user = 'admin'
#password = 'admin'
#enable_password = '1234'
#addr = '192.168.1.231'
#port = '22'
#method = 'ssh'
#cmd = ['interface print','ip address print','ip arp print']
#
#
#txt = connector.mtlogin(user,addr,cmd,password,enable_password,port,method)
#print(txt)
#
## Сохранение вывода в файл
#file_path = 'test.py'
#with open(file_path, 'w') as file:
#    file.write(txt)
#
#
#arr = connector.version(user,addr,password,enable_password)
#print(arr)
#
#################################################
